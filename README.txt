
Description
-----------

 This module uses AEMET data from their website to display current weather conditions from
 almost 8112 cities of Spain. ACCORDING TO THE COPYRIGHT DATA, THIS DATA CAN BE USED IF AEMET IS QUOTED AS THE DATA
 AUTHOR (like this module does), SO DON'T CHANGE THIS.

Features
--------

 -Supports an "unlimited" number of locations per block.
 -Provides a custom block which can be administrated by a user.
 -Download of current AEMET data is scheduled in a smart way
  to reduce unnecessary network traffic and keep your site
  responsive.
 -Fully translatable, currently available in English, Spanish and Catalan.
 -Works well with MySQLdatabases, it's suppose to work on PostgreSQL databases too but I don't know it.

Author
------
 Jos� M� Sirvent <drupal@hykrion.com>

 Weather_es was inspired by the Weather module which was written by Tobias Quathamer <t.quathamer@gmx.net>.
 Thanks to http://kutxa.homeunix.org/ for his theme, called solarized.
 Thanks to Garikoitz Itxaso for his translation to Euskera.
 Thanks to Sergio Caridad for his translation to Galician.
