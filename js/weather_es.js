/**
 * @author jmsirvent
 */

/**
 * Global Object for Namespace
 */
var Weather_es = Weather_es || {};
/**
 * Drupal behaviors
 */
Drupal.behaviors.weather_es = function(context){
  //$('.weather_es p strong').css('background-color', 'blue');
  // Get the days with her info
  var days = $('.weather_es');
  // Init the object
  Weather_es.days = days;
  Weather_es.page = 1;

  Weather_es.pager();
  $('a.active').click(function(){
    Weather_es.page = $(this).attr('rel');
    Weather_es.pager();
  });
  //setInterval(Weather_es.periodicRefresh, 10000);
}
/**
 * Handle the visibility of the diferent days
 */
Weather_es.pager = function(){
  var page = Weather_es.page;
  var days = Weather_es.days;

  days.each(function(){
    var rel = $(this).attr('rel');
    if (rel != page){
      $(this).hide();
      //$(this).fadeOut('slow');
      //$(this).slideUp('slow');
    }
    else{
      $(this).show();
      //$(this).fadeIn('slow');
      //$(this).slideDown('slow');
    }

  });
  $(".weather_es-pager-item").each(function(){
    if (parseInt($(this).children('a').attr('rel')) == page){
      $(this).addClass('pager-current');
    }
    else{
    	$(this).removeClass('pager-current');
    }
  })
};
/**
 * Periodical refresh (optional)
 */
Weather_es.periodicRefresh = function(){
  var days = Weather_es.days;
  var page = Weather_es.page;
  var lastPage = 4;

  Weather_es.pager();
  if (page == lastPage){
    Weather_es.page = 1;
  }
  else{
    Weather_es.page = page + 1;
  }
};
